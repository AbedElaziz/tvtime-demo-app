TVTimeDemo
=====

This is a simple Android application that shows a list of Popular TV Shows & movies. For the purpose
of this demo, I developed the app using both Android & Kotlin. The following briefly describes the
components used to build the demo;

* **RXJava**: all asynchronous tasks where implemented through the use of RXJava
* **Dagger2**: dependency injection to supply classes with dependencies needed to perform various operations
* **Retrofit**: networking in this demo was done using this library
* **Butterknife**: for field and method binding for the views

The architecture I followed in this project is Model-View-Presenter (MVP) where model represents our
data source - which is remote data repository through api calls in this case-. View is the activities
in this project and presenter is where the logic resides and what acts like a bridge between View and
Model. The simplest why to describe this in our project is that when user pull to refresh the list
from View, the presenter acts upon that by asking the model for data, do the filtering/mapping logic
and pass the data to be displayed on the view.


#### Screenshots

![Scheme](images/screen1.png)
![Scheme](images/screen2.png)

#### Notes

     - The application was compiled on Android Studio version 3.5.1
     - The application is using androidx artifacts
     - Both Java 8 and kotlin were used to build the app
     - Dependencies and project configurations can be found under buildSrc

Project breakdown
-------------

This section is to list down all packages that can be found in the project
* **api**: it contains api calls services that specifies call methods and call parameters
* **base**: this package has base activity and base presenter classes
* **di**: it stands for Dependency Injection where dependencies like retrofit is created and then exposed
to be injected/provided in various activities ready to be used.
* **mvp**: it contains views and presenters described above
* **ui**: ui in this project has activities and adapters
* **utils**: helper classes such as network checker and recycler views configuration classes

### How to make it better

Butterknife can be replaced with Data Binding for better performance and an offline repository can be
created to always show data even though if there is no internet connection.