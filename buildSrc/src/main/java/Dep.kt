//Declare all dependencies needed for the app
object Dep{

    object Version{
        const val buildGradle = "3.5.1"
        const val kotlin = "1.3.41"
        const val junit = "4.12"
        const val junitTest = "1.1.1"
        const val espresso = "3.2.0"
        const val androidX = "1.0.2"
        const val dagger = "2.23.2"
        const val retrofit = "2.6.2"
        const val loggingInterceptor = "4.2.1"
        const val recyclerView = "1.0.0"
        const val materia = "1.1.0-alpha07"
        const val supportDesign = "28.0.0"
        const val butterKnife = "10.2.0"
        const val glide = "4.10.0"
        const val rxJava = "1.3.0"
        const val rxAndroid = "0.24.0"
        const val constraintlayout = "1.1.3"

    }

    //plugin
    const val pluginBuildGradle = "com.android.tools.build:gradle:${Version.buildGradle}"
    const val pluginKotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kotlin}"

    //kotlin
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.kotlin}"

    //RX
    const val rxJava = "io.reactivex:rxjava:${Version.rxJava}"
    const val rxAndroid = "io.reactivex:rxandroid:${Version.rxAndroid}"

    //UI
    const val recyclerView =  "androidx.recyclerview:recyclerview:${Version.recyclerView}"
    const val materia =  "com.google.android.material:material:${Version.materia}"
    const val supportDesign = "com.android.support:design:${Version.supportDesign}"
    const val constraintlayout = "androidx.constraintlayout:constraintlayout:${Version.constraintlayout}"

    //Image Loader
    const val glide = "com.github.bumptech.glide:glide:${Version.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Version.glide}"

    //Butter knife
    const val butterKnife = "com.jakewharton:butterknife:${Version.butterKnife}"
    const val butterKnifeProcessor = "com.jakewharton:butterknife-compiler:${Version.butterKnife}"

    //androidx
    const val appcompat = "androidx.appcompat:appcompat:${Version.androidX}"
    //val corex = "androidx.core:core-ktx:${Version.androidX}"

    //Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
    const val convertorJackson =  "com.squareup.retrofit2:converter-jackson:${Version.retrofit}"
    const val retrofitRXAdapter =  "com.squareup.retrofit2:adapter-rxjava:${Version.retrofit}"
    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Version.loggingInterceptor}"

    //di
    const val dagger = "com.google.dagger:dagger:${Version.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Version.dagger}"
    //val daggerAndroid= "com.google.dagger:dagger-android:${Version.dagger}"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:${Version.dagger}"
    const val daggerAndroidProcessor = "com.google.dagger:dagger-android-processor:${Version.dagger}"

    //test
    const val junit =  "junit:junit:${Version.junit}"
    const val junitTest =  "androidx.test.ext:junit:${Version.junitTest}"
    const val espresso = "androidx.test.espresso:espresso-core:${Version.espresso}"

}