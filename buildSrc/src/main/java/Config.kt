object Config{
    const val versionName = "1.0.0"
    const val versionCode = 1
    const val packageName = "bjak.my.demo"
    const val compileSdkVersion = 28
    const val targetSdkVersion = 28
    const val minSdkVersion = 21
}