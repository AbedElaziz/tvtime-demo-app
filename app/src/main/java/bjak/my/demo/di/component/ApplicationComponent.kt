package bjak.my.demo.di.component

import android.content.Context
import bjak.my.demo.di.module.ApplicationModule
import dagger.Component
import retrofit2.Retrofit
import java.lang.StringBuilder
import javax.inject.Singleton

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This interface it to expose/provide dependencies declared at ApplicationModule.class
 */

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun exposeRetrofit(): Retrofit
    fun exposeContext(): Context
    fun exposeStringBuilder(): StringBuilder
}