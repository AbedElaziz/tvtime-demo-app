package bjak.my.demo.di.module

import bjak.my.demo.api.ApiService
import bjak.my.demo.di.scope.PerActivity
import bjak.my.demo.mvp.view.IFeedDetailsContract
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class FeedDetailsModule(private var view : IFeedDetailsContract.IFeedDetailsView){

    @PerActivity
    @Provides
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @PerActivity
    @Provides
    fun provideView() : IFeedDetailsContract.IFeedDetailsView = view

}