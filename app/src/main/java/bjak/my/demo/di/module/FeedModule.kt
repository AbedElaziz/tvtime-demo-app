package bjak.my.demo.di.module

import bjak.my.demo.api.ApiService
import bjak.my.demo.di.scope.PerActivity
import bjak.my.demo.mvp.view.IFeedsContract
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class FeedModule(private var view : IFeedsContract.IFeedsView){

    @PerActivity
    @Provides
    internal fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @PerActivity
    @Provides
    fun provideView() : IFeedsContract.IFeedsView = view

}