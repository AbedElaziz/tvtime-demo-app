package bjak.my.demo.di.component

import bjak.my.demo.di.module.FeedModule
import bjak.my.demo.di.scope.PerActivity
import bjak.my.demo.ui.activity.FeedsActivity
import dagger.Component

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * inject dependency in FeedsActivity
 */

@PerActivity
@Component(modules = [FeedModule::class], dependencies = [ApplicationComponent::class])
interface FeedsComponent {

    fun inject(activity: FeedsActivity)
}