package bjak.my.demo.di.module

import android.content.Context
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This class used for dependency injections to provide singelton references of dependency anywhere needed
 * in the app
 *
 * The most important dependency here in retrofit which is used for networking and called at perActivity
 * scope for each activity or fragment calling it
 */

@Module
class ApplicationModule(private val mContext: Context, private val mBaseUrl: String) {


    // Context
    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return mContext
    }

    // Logging
    @Provides
    @Singleton
    internal fun provideInterceptor(): Interceptor {

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        return httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }

    }

    @Provides
    @Singleton
    internal fun provideDispatcher(): Dispatcher {
        return Dispatcher()
    }

    //Http client
    @Provides
    @Singleton
    internal fun provideOkHttpClient(interceptor: Interceptor, dispatcher: Dispatcher): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .dispatcher(dispatcher)
                .build()
    }

    @Singleton
    @Provides
    internal fun provideObjectMapper(): ObjectMapper {
        return ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    @Provides
    @Singleton
    internal fun provideJaksonConverterFactory(objectMapper: ObjectMapper): JacksonConverterFactory {
        return JacksonConverterFactory.create(objectMapper)
    }

    @Singleton
    @Provides
    internal fun provideRxJavaCallAdapterFactory(): RxJavaCallAdapterFactory {
        return RxJavaCallAdapterFactory.create()
    }

    //retrofit
    @Provides
    @Singleton
    internal fun provideRetrofit(client: OkHttpClient, converterFactory: JacksonConverterFactory,
                                 adapterFactory: RxJavaCallAdapterFactory): Retrofit {

        return Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(adapterFactory)
                .client(client)
                .build()
    }

    @Singleton
    @Provides
    internal fun provideStringBuilder(): StringBuilder  =  StringBuilder()


}
