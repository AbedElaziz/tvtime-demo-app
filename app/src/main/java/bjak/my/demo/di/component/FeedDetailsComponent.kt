package bjak.my.demo.di.component

import bjak.my.demo.di.module.FeedDetailsModule
import bjak.my.demo.di.scope.PerActivity
import bjak.my.demo.ui.activity.FeedDetailsActivity
import dagger.Component

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * inject dependency in FeedDetailsActivity
 */

@PerActivity
@Component(modules = [FeedDetailsModule::class], dependencies = [ApplicationComponent::class])
interface FeedDetailsComponent {

    fun inject(activity: FeedDetailsActivity)
}