package bjak.my.demo.utils

import android.app.Activity
import android.content.Context
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


//Configure linear recycler View
fun configureRecyclerView(recyclerView: RecyclerView, activity: Activity): RecyclerView {
    recyclerView.setHasFixedSize(true)
    recyclerView.setRecycledViewPool(RecyclerView.RecycledViewPool())
    recyclerView.layoutManager = LinearLayoutManager(activity)
    recyclerView.itemAnimator = DefaultItemAnimator()

    return recyclerView
}

//Configure horizontal recycler View
fun configureHorizontalRecyclerView(recyclerView: RecyclerView, context: Context): RecyclerView {
    recyclerView.setHasFixedSize(true)
    recyclerView.setRecycledViewPool(RecyclerView.RecycledViewPool())
    recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    recyclerView.itemAnimator = DefaultItemAnimator()

    return recyclerView
}