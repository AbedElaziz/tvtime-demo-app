package bjak.my.demo.utils

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This file contains constants values
 */


object Constants{

    const val BASE_URL = "https://cdn-discover.hooq.tv/v1.2/"
    const val DISCOVER_TITLES = "discover/titles/"
    const val DISCOVER_FEED = "discover/feed"

    //required type to show
    const val FEED_TYPE = "Multi-Title-Manual-Curation"
    const val POSTER_TYPE = "POSTER"
    const val BACKGROUND_TYPE = "BACKGROUND"
    const val MOVIE = "MOVIE"

    const val TOTAL_ITEMS_PER_PAGE = 20

}