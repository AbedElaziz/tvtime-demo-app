package bjak.my.demo.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import bjak.my.demo.R
import com.google.android.material.snackbar.Snackbar

/**
 * Method to display SnackBar with short duration
 * @param view - View type (Eg: TextView, Button, EditText)
 * @param text - The text that should be shown on the Snackbar
 */
fun showSnackMessage(view: View, text: String)  =  Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()


fun showNetworkSnackMessage(view: View, context: Context): Snackbar {
    val snackbar = Snackbar.make(view, context.getString(R.string.noConnection), Snackbar.LENGTH_INDEFINITE)
    snackbar.setAction("Dismiss") { snackbar.dismiss() }
    return snackbar

}

/**
 * Method to show toast message
 * @param context Context Of Activity
 * @param message Message to display in the toast
 */
fun showToastMessage(context: Context, message: String) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
