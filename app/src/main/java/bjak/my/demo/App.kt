package bjak.my.demo

import android.app.Application

import bjak.my.demo.di.component.ApplicationComponent
import bjak.my.demo.di.component.DaggerApplicationComponent
import bjak.my.demo.di.module.ApplicationModule

import bjak.my.demo.utils.Constants.BASE_URL
/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
  */
class App : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        initializeApplicationComponent()
    }

    private fun initializeApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this, BASE_URL))
                .build()
    }



}
