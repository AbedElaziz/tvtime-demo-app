package bjak.my.demo.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import bjak.my.demo.R;
import bjak.my.demo.di.module.GlideApp;
import bjak.my.demo.ui.model.FeedsData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HorizontalTitlesAdapter extends RecyclerView.Adapter<HorizontalTitlesAdapter.ViewHolder> {


    private List<FeedsData> data;
    private Context context;
    private OnItemClickedListener onItemClickedListener;


    HorizontalTitlesAdapter(Context context, List<FeedsData> data, OnItemClickedListener onItemClickedListener) {
        this.data = data;
        this.context = context;
        this.onItemClickedListener = onItemClickedListener;

    }

    @NonNull
    @Override
    public HorizontalTitlesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_titles_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalTitlesAdapter.ViewHolder holder, int position) {
        if (data.size() > 0)
            holder.bindView(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.picture)
        ImageView picture;

        @BindView(R.id.title)
        TextView title;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(FeedsData data) {

            GlideApp.with(context)
                    .load(data.getUrl())
                    .placeholder(R.drawable.ic_photo_24dp)
                    .into(picture);

            title.setText(data.getTitle() != null ? data.getTitle() : "No Title");

            itemView.setOnClickListener(v-> onItemClickedListener.onClicked(data.getId()));
        }

    }

    public interface OnItemClickedListener{
        void onClicked(String id);
    }

}
