package bjak.my.demo.ui.model

data class FeedDataModel(
        val row_id: String,
        val row_name: String? = null,
        val order: Int,
        val type: String,
        val data: List<FeedsData>? = null
)


data class FeedsData(
        val id: String,
        val title: String? = null,
        val itemId: String = "",
        val url: String = "",
        val type: String = ""
)
