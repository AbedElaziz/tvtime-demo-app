package bjak.my.demo.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bjak.my.demo.R;
import bjak.my.demo.base.BaseActivity;
import bjak.my.demo.di.component.DaggerFeedDetailsComponent;
import bjak.my.demo.di.module.FeedDetailsModule;
import bjak.my.demo.di.module.GlideApp;
import bjak.my.demo.mvp.model.FeedDetails;
import bjak.my.demo.mvp.model.Images;
import bjak.my.demo.mvp.model.Tags;
import bjak.my.demo.mvp.presenter.FeedDetailsPresenter;
import bjak.my.demo.mvp.view.IFeedDetailsContract;
import bjak.my.demo.ui.adapter.LanguagesAdapter;
import bjak.my.demo.utils.MessageUtilsKt;
import bjak.my.demo.utils.NetworkUtil;
import butterknife.BindView;

import static bjak.my.demo.utils.Constants.BACKGROUND_TYPE;
import static bjak.my.demo.utils.Constants.MOVIE;
import static bjak.my.demo.utils.RecyclerViewConfigurationKt.configureRecyclerView;

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This activity shows details of a feed by its id, it extends base activity
 */
public class FeedDetailsActivity extends BaseActivity implements IFeedDetailsContract.IFeedDetailsView {

    //UI
    @BindView(R.id.wrapperView)
    protected ViewGroup wrapperView;

    @BindView(R.id.contentsLayout)
    protected ViewGroup contentsLayout;

    @BindView(R.id.emptyViewLayout)
    protected ViewGroup emptyViewLayout;

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @BindView(R.id.headerImage)
    protected ImageView headerImage;

    @BindView(R.id.back)
    protected ImageView back;

    @BindView(R.id.title)
    protected TextView title;

    @BindView(R.id.seasons)
    protected TextView seasons;

    @BindView(R.id.dateGenre)
    protected TextView dateGenre;

    @BindView(R.id.description)
    protected TextView description;

    @BindView(R.id.readMore)
    protected ViewGroup readMore;

    @BindView(R.id.languagesList)
    protected RecyclerView languagesList;

    //Adapter
    private RecyclerView.Adapter languagesAdapter;

    private String id;

    private List<String> languages = new ArrayList<>();

    @Inject
    FeedDetailsPresenter presenter;

    @Inject
    StringBuilder stringBuilder;

    //To start the activity
    public static void start(Context activity, String id) {
        Intent intent = new Intent(activity, FeedDetailsActivity.class);
        intent.putExtra("id", id);
        activity.startActivity(intent);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_feed_details;
    }

    @Override
    public void onViewReady(Bundle savedInstanceState, @NotNull Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        if (intent.getExtras() != null) {
            id = intent.getExtras().getString("id", "");
        }

        //back
        back.setOnClickListener(v -> finish());

        //set languages list
        setLanguagesList();

        //Load Details
        loadDetails();

        handleReadMoreForLongDescription();
    }

    private void setLanguagesList() {
        configureRecyclerView(languagesList, this);
        languagesAdapter = new LanguagesAdapter(languages);
        languagesList.setAdapter(languagesAdapter);
    }

    private void loadDetails() {
        if (NetworkUtil.INSTANCE.isConnected(this))
            presenter.fetchFeedDetails(id);
        else showNoConnection(true);
    }

    private void handleReadMoreForLongDescription() {
        description.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                // Remove listener because we don't want this called before _every_ frame
                if (description.getLineCount() > 1) {
                    description.getViewTreeObserver().removeOnPreDrawListener(this);
                }
                readMore.setVisibility(description.getLineCount() >= 8 ? View.VISIBLE : View.GONE);

                return true;
            }
        });

        readMore.setOnClickListener(v -> {
            description.setMaxLines(50);
            readMore.setVisibility(View.GONE);
        });
    }

    @Override
    public void onFeedDetailsFetchedSuccessfully(@NotNull FeedDetails response) {


        // Handle header image
        String imageUrl = "";
        if (response.getImages() != null && response.getImages().size() > 0) {
            for (Images images : response.getImages()) {
                if (images.getType().equals(BACKGROUND_TYPE)) {
                    imageUrl = images.getUrl();
                    break;
                }
            }

            GlideApp.with(this)
                    .load(imageUrl)
                    .fallback(R.drawable.ic_photo_24dp)
                    .error(R.drawable.ic_photo_24dp)
                    .into(headerImage);
        }

        // Handle Title
        title.setText(response.getTitle());

        // seasons
        String seasonsValue = "";
        if (response.getAs().equals(MOVIE)) {
            seasonsValue = getString(R.string.movie);
        } else {
            if (response.getSeasons() != null)
                seasonsValue = response.getSeasons().size() + " " + getString(R.string.seasons);

        }
        seasons.setText(seasonsValue);

        // Date-genre
        stringBuilder.setLength(0);
        for (Tags tags : response.getTags()) {
            if (stringBuilder.length() > 0 && !tags.getLabel().isEmpty())
                stringBuilder.append(", ");
            stringBuilder.append(tags.getLabel());
        }

        String dateGenreValue = response.getMeta().getReleaseYear() + " - " + stringBuilder.toString();
        dateGenre.setText(dateGenreValue);

        //Description
        description.setText(response.getDescription());

        languages.clear();
        if(response.getLanguages().size() > 0)
        languages.addAll(response.getLanguages());
        else languages.add("N/A");
        languagesAdapter.notifyDataSetChanged();

        contentsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFeedDetailsFetchFailure(@NotNull String errorMessage) {
        MessageUtilsKt.showToastMessage(this, errorMessage);
    }

    @Override
    public void showProgress() {
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar != null) progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyView(boolean show) {
        if (emptyViewLayout != null) emptyViewLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoConnection(boolean show) {
        if (show)
            MessageUtilsKt.showNetworkSnackMessage(wrapperView, this).show();
    }

    //Resolve declared dependencies so they can be injected in this class
    @Override
    protected void resolveDaggerDependency() {

        DaggerFeedDetailsComponent.builder()
                .applicationComponent(getApplicationComponents())
                .feedDetailsModule(new FeedDetailsModule(this))
                .build()
                .inject(this);

    }
}
