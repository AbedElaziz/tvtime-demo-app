package bjak.my.demo.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import bjak.my.demo.R;
import bjak.my.demo.ui.model.FeedDataModel;
import bjak.my.demo.ui.model.FeedsData;
import butterknife.BindView;
import butterknife.ButterKnife;

import static bjak.my.demo.utils.RecyclerViewConfigurationKt.configureHorizontalRecyclerView;

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * Feeds list adapter
 */

public class FeedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int FOOTER_VIEW = 1;
    private List<FeedDataModel> feedsData;
    private Context context;

    private FooterViewHolder footerViewHolder;
    private OnFeedClickedListener onFeedClickedListener;


    public FeedsAdapter(Context context, List<FeedDataModel> feedsData, OnFeedClickedListener onFeedClickedListener) {
        this.feedsData = feedsData;
        this.context = context;
        this.onFeedClickedListener = onFeedClickedListener;
    }

    //show/hide progress bar for loading more at the bottom/fotter of the list
    public void setUpdateMore(boolean updateMore) {
        footerViewHolder.footerLoadingLayout.setVisibility(updateMore ? View.VISIBLE : View.GONE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        if (viewType == FOOTER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_layout, parent, false);
            return new FooterViewHolder(view);

        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_row_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder normalViewHolder = (ItemViewHolder) holder;
            if (feedsData.size() > 0)
                normalViewHolder.bindView(feedsData.get(position));
        } else if (holder instanceof FooterViewHolder) {
            footerViewHolder = (FooterViewHolder) holder;
            footerViewHolder.footerLoadingLayout.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        if (feedsData == null) {
            return 0;
        }

        if (feedsData.size() == 0) {
            return 1;
        }
        return feedsData.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == feedsData.size()) {
            return FOOTER_VIEW;
        }
        return super.getItemViewType(position);
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.footerLoadingLayout)
        ViewGroup footerLoadingLayout;

        FooterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder  implements HorizontalTitlesAdapter.OnItemClickedListener {
        @BindView(R.id.headerTitle)
        TextView headerTitle;

        @BindView(R.id.titlesList)
        RecyclerView titlesList;



        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            configureHorizontalRecyclerView(titlesList, context);

        }

        void bindView(FeedDataModel feedsData) {
            if (feedsData.getData() != null) {
                List<FeedsData> feedItems = new ArrayList<>(feedsData.getData());
                headerTitle.setText(feedsData.getRow_name());
                HorizontalTitlesAdapter adapter = new HorizontalTitlesAdapter(context, feedItems, this);
                titlesList.setAdapter(adapter);

            }

        }

        @Override
        public void onClicked(String id) {
            onFeedClickedListener.onFeedClicked(id);
        }
    }

   public interface OnFeedClickedListener{
        void onFeedClicked(String id);
   }
}
