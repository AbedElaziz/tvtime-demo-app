package bjak.my.demo.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import bjak.my.demo.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguagesAdapter extends RecyclerView.Adapter<LanguagesAdapter.ViewHolder> {

    private List<String> data;


    public LanguagesAdapter(List<String> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public LanguagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_row_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguagesAdapter.ViewHolder holder, int position) {

        if (data.size() > 0)
            holder.bindView(data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.headerTitle)
        TextView headerTitle;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(String data) {

            headerTitle.setText(data);
        }
    }

}
