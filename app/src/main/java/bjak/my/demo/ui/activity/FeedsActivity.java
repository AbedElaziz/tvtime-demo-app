package bjak.my.demo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bjak.my.demo.R;
import bjak.my.demo.base.BaseActivity;
import bjak.my.demo.di.component.DaggerFeedsComponent;
import bjak.my.demo.di.module.FeedModule;
import bjak.my.demo.mvp.presenter.FeedsPresenter;
import bjak.my.demo.mvp.view.IFeedsContract;
import bjak.my.demo.ui.adapter.FeedsAdapter;
import bjak.my.demo.ui.model.FeedDataModel;
import bjak.my.demo.utils.MessageUtilsKt;
import bjak.my.demo.utils.NetworkUtil;
import butterknife.BindView;

import static bjak.my.demo.utils.Constants.TOTAL_ITEMS_PER_PAGE;
import static bjak.my.demo.utils.RecyclerViewConfigurationKt.configureRecyclerView;

public class FeedsActivity extends BaseActivity implements IFeedsContract.IFeedsView, FeedsAdapter.OnFeedClickedListener {

    // UI
    @BindView(R.id.wrapperView)
    protected ViewGroup wrapperView;

    @BindView(R.id.swipeToRefresh)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.feedsList)
    protected RecyclerView feedsList;

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @BindView(R.id.emptyViewLayout)
    protected ViewGroup emptyViewLayout;

    // RecyclerView Adapter
    private FeedsAdapter adapter;

    private List<FeedDataModel> feedsData = new ArrayList<>();
    private int totalPages;
    private int currentPage = 1;

    // call presenter
    @Inject
    FeedsPresenter presenter;
    private boolean isRefresh = false;
    private Snackbar snackbar;


    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady(@Nullable Bundle savedInstanceState, @NotNull Intent intent) {
        super.onViewReady(savedInstanceState, intent);

        //setup recyclerView
        setupFeedsList();

        //load feeds (current page is 1)
        loadFeeds(1, false);

        //swipe to refresh data
        setupSwipeToRefresh();
    }

    private void setupFeedsList() {
        //Call to utility file to configure recycler view
        configureRecyclerView(feedsList, this);
        adapter = new FeedsAdapter(getApplicationContext(), feedsData, this);
        feedsList.setAdapter(adapter);


        //When end of list is reached, check if you can load more items
        feedsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (recyclerView.getLayoutManager() != null) {
                    if (!isRefresh) {
                        int visibleItemCount = recyclerView.getChildCount();
                        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                        int pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                        if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                            //End of list
                            if (currentPage < totalPages) {
                                adapter.setUpdateMore(true);
                                currentPage++;
                                loadFeeds(currentPage, false);
                            }
                        }
                    }
                }


            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
    }

    private void setupSwipeToRefresh() {

        swipeRefreshLayout.setOnRefreshListener(() -> {
            showNoConnection(false);
            isRefresh = true;
            currentPage = 1;
            loadFeeds(currentPage, true);
            isRefresh = false;
        });
    }

    private void loadFeeds(int currentPage, boolean isRefresh) {
        //check for internet connection then make the call
        if (NetworkUtil.INSTANCE.isConnected(this))
            presenter.fetchFeedsList("ID", currentPage, TOTAL_ITEMS_PER_PAGE, isRefresh);
        else showNoConnection(true);
    }

    @Override
    public void onFeedsListFetchedSuccessfully(@NotNull List<FeedDataModel> allFeedsList, int totalPages, int currentPage) {
        if (allFeedsList.size() > 0) {

            //This means fresh load of the list
            if (this.currentPage == 1) {
                feedsData.clear();
            }

            //total pages
            this.totalPages = totalPages;
            //new current page
            this.currentPage = currentPage;

            feedsData.addAll(allFeedsList);
            adapter.notifyDataSetChanged();


            showEmptyView(false);
        } else {
            showEmptyView(true);
        }
    }

    @Override
    public void onFeedClicked(String id) {
        FeedDetailsActivity.start(this, id);
    }

    @Override
    public void onFeedListFetchFailure(@NotNull String errorMessage) {
        MessageUtilsKt.showToastMessage(this, errorMessage);
    }

    @Override
    public void showProgress() {
        if (progressBar != null) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (progressBar != null) progressBar.setVisibility(View.GONE);
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showEmptyView(boolean show) {
        if (emptyViewLayout != null) emptyViewLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoConnection(boolean show) {
        if (show) {
            snackbar = MessageUtilsKt.showNetworkSnackMessage(wrapperView, this);
            snackbar.show();
        }
        else {
            if(snackbar != null && snackbar.isShown()) snackbar.dismiss();
        }
    }

    @Override
    protected void resolveDaggerDependency() {
        DaggerFeedsComponent.builder()
                .applicationComponent(getApplicationComponents())
                .feedModule(new FeedModule(this))
                .build()
                .inject(this);
    }


}
