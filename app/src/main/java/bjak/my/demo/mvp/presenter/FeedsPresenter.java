package bjak.my.demo.mvp.presenter;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import bjak.my.demo.R;
import bjak.my.demo.api.ApiService;
import bjak.my.demo.base.BasePresenter;
import bjak.my.demo.mvp.model.FeedData;
import bjak.my.demo.mvp.model.FeedDataData;
import bjak.my.demo.mvp.model.FeedDataDataImages;
import bjak.my.demo.mvp.model.FeedResponse;
import bjak.my.demo.mvp.view.IFeedsContract;
import bjak.my.demo.ui.model.FeedDataModel;
import bjak.my.demo.ui.model.FeedsData;
import rx.Observable;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static bjak.my.demo.utils.Constants.FEED_TYPE;
import static bjak.my.demo.utils.Constants.POSTER_TYPE;

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 * <p>
 * FeedsPresenter acts like a medium between view(ui/controllers) and model(data sources). It listens
 * to UI operations like clicks or refreshes, handles the business logic and notify the view
 * of changes like fetching data from remote repository in our case
 */

public class FeedsPresenter extends BasePresenter<IFeedsContract.IFeedsView> implements IFeedsContract.IPresenter {

    private IFeedsContract.IFeedsView mView;

    @Inject
    ApiService apiService;

    @Inject
    Context context;

    //RX subscription
    private Subscription subscription;


    @Inject
    public FeedsPresenter(IFeedsContract.IFeedsView mView) {
        super(mView);
        this.mView = mView;
    }

    /**
     * This api call to fetch feeds list
     *
     * @param region  region id
     * @param page    page number - for pagination purpose
     * @param perPage number of items per page
     */
    @Override
    public void fetchFeedsList(@NotNull String region, int page, int perPage, boolean isRefresh) {
        //show progress
        if (page == 1 && !isRefresh)
            mView.showProgress();

        Single<FeedResponse> feedObservable = apiService.getFeeds(region, page, perPage);

        subscription = feedObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleSubscriber<FeedResponse>() {
                    @Override
                    public void onSuccess(FeedResponse feedResponse) {
                        if (feedResponse != null && feedResponse.getData().size() > 0) {
                            Observable.fromCallable(() -> {
                                List<FeedDataModel> allFeedsList = new ArrayList<>();
                                for (FeedData feedData : feedResponse.getData()) {
                                    if (FEED_TYPE.equals(feedData.getType())) {
                                        if (feedData.getRow_name() != null && feedData.getData() != null) {

                                            List<FeedsData> feedsDataList = new ArrayList<>();
                                            if (feedData.getData() != null && feedData.getType().equals(FEED_TYPE)) {
                                                for (FeedDataData _f2 : feedData.getData()) {
                                                    if (_f2.getImages() != null)
                                                        for (FeedDataDataImages images : _f2.getImages()) {
                                                            if (images.getType().equals(POSTER_TYPE))
                                                                feedsDataList.add(new FeedsData(
                                                                        _f2.getId(), _f2.getTitle(),
                                                                        images.getId(),
                                                                        images.getUrl(),
                                                                        images.getType()
                                                                ));
                                                        }
                                                }


                                                allFeedsList.add(new FeedDataModel(
                                                        feedData.getRow_id(),
                                                        feedData.getRow_name(),
                                                        feedData.getOrder(),
                                                        feedData.getType(),
                                                        feedsDataList
                                                ));
                                            }

                                        }

                                    }
                                }

                                return allFeedsList;
                            })
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe((allFeedsList) -> {
                                        //on success, notify view and pass the response
                                        mView.onFeedsListFetchedSuccessfully(allFeedsList, feedResponse.getPagination().getTotalPages(), feedResponse.getPagination().getPage());
                                        mView.showEmptyView(false);
                                        mView.hideProgress();
                                    });


                        } else {
                            //Notify the view of an error and hide progress
                            mView.onFeedListFetchFailure(context.getString(R.string.error));
                            mView.hideProgress();
                            mView.showEmptyView(true);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        mView.onFeedListFetchFailure(error.getMessage());
                        mView.hideProgress();
                        mView.showEmptyView(true);
                    }
                });
    }

    @Override
    public void stop() {
        unSubscribe();
    }

    private void unSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
