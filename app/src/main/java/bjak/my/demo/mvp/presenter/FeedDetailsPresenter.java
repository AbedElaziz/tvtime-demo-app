package bjak.my.demo.mvp.presenter;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import bjak.my.demo.R;
import bjak.my.demo.api.ApiService;
import bjak.my.demo.base.BasePresenter;
import bjak.my.demo.mvp.model.FeedDetailsResponse;
import bjak.my.demo.mvp.view.IFeedDetailsContract;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 *  FeedDetailsPresenter acts like a medium between view(ui/controllers) and model(data sources). It listens
 *  to UI operations like clicks or refreshes, handles the business logic and notify the view
 *  of changes like fetching data from remote repository in our case
 */

public class FeedDetailsPresenter extends BasePresenter<IFeedDetailsContract.IFeedDetailsView> implements IFeedDetailsContract.IPresenter {

    private IFeedDetailsContract.IFeedDetailsView mView;

    @Inject
    ApiService apiService;

    @Inject
    Context context;

    //RX subscription
    private Subscription subscription;

    @Inject
    public FeedDetailsPresenter(IFeedDetailsContract.IFeedDetailsView mView) {
        super(mView);
        this.mView = mView;
    }

    /**
     * This api call to fetch feed details
     * @param id id of the feed to be fetched
     */
    @Override
    public void fetchFeedDetails(@NotNull String id) {
        //show progress
        mView.showProgress();

        Single<FeedDetailsResponse> feedObservable = apiService.getFeedDetails(id);

        subscription = feedObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleSubscriber<FeedDetailsResponse>() {
                    @Override
                    public void onSuccess(FeedDetailsResponse feedResponse) {
                        if(feedResponse != null){
                            //on success, notify view and pass the response
                            mView.onFeedDetailsFetchedSuccessfully(feedResponse.getData());
                            mView.hideProgress();
                        }else{
                            //Notify the view of an error and hide progress
                            mView.onFeedDetailsFetchFailure(context.getString(R.string.error));
                            mView.hideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        mView.onFeedDetailsFetchFailure(error.getMessage());
                        mView.hideProgress();
                    }
                });
    }


    @Override
    public void stop() {
        unSubscribe();
    }

    private void unSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
