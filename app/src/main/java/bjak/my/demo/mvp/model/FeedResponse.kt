package bjak.my.demo.mvp.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is a model class to parse json response for the list of feeds. Here only the important data is parsed that is
 * needed for the demo. Others are ignored
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class FeedResponse(
        @JsonProperty("data") val data: List<FeedData>,
        @JsonProperty("pagination") val pagination: FeedPagination

)


data class FeedData(
        @JsonProperty("row_id") val row_id: String,
        @JsonProperty("row_name") val row_name: String? = null,
        @JsonProperty("order") val order: Int,
        @JsonProperty("type") val type: String,
        @JsonProperty("data") val data: List<FeedDataData>? = null
)


data class FeedDataData(
        @JsonProperty("id") val id: String,
        @JsonProperty("title") val title: String? = null,
        @JsonProperty("images") val images: List<FeedDataDataImages>? = null
)

data class FeedDataDataImages(
        @JsonProperty("id") val id: String,
        @JsonProperty("url") val url: String,
        @JsonProperty("type") val type: String
)

data class FeedPagination(
        @JsonProperty("page") val page: Int,
        @JsonProperty("perPage") val perPage: Int,
        @JsonProperty("totalPages") val totalPages: Int,
        @JsonProperty("totalCount") val totalCount: Int
)

