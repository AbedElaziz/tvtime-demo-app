package bjak.my.demo.mvp.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is a model class to parse json response for the details of each movie/show id passed. Here only the important data is parsed that is
 * needed for the demo. Others are ignored
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class FeedDetailsResponse(
        @JsonProperty("data") val data: FeedDetails
)

data class FeedDetails(
        @JsonProperty("title") val title: String? = null,
        @JsonProperty("description") val description: String,
        @JsonProperty("short_description") val short_description: String,
        @JsonProperty("as") val `as`: String,
        @JsonProperty("languages") val languages: List<String>,
        @JsonProperty("audios") val audios: List<String>,
        @JsonProperty("images") val Images: List<Images>? = null,
        @JsonProperty("tags") val tags: List<Tags>,
        @JsonProperty("seasons") val seasons: List<Int>? = null,
        @JsonProperty("meta") val meta: Meta


)

data class Images(
        @JsonProperty("id") val id: String,
        @JsonProperty("url") val url: String,
        @JsonProperty("type") val type: String
)

data class Tags(
        @JsonProperty("label") val label: String
)

data class Meta(
        @JsonProperty("releaseYear") val releaseYear: Int
)