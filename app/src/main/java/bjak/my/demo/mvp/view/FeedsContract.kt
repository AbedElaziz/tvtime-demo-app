package bjak.my.demo.mvp.view

import bjak.my.demo.ui.model.FeedDataModel

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is an interface contact where View and data model interacts indirectly through a bridge (presenter)
 */
interface IFeedsContract {

    //View
    interface IFeedsView : BaseView {

        fun onFeedsListFetchedSuccessfully(allFeedsList : List<FeedDataModel>, totalPages: Int, currentPage: Int)

        fun onFeedListFetchFailure(errorMessage: String)
    }

    //presenter
    interface IPresenter {
        fun fetchFeedsList(region: String, page: Int, perPage: Int, isRefresh: Boolean)
    }

}