package bjak.my.demo.mvp.view

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * Base view interface. For this demo, the only common functions for each view is to show/hide
 * progress indicator when the data is being fetched from server
 */
interface BaseView{
    fun showProgress()
    fun hideProgress()
    fun showEmptyView(show: Boolean)
    fun showNoConnection(show: Boolean)
}