package bjak.my.demo.mvp.view

import bjak.my.demo.mvp.model.FeedDetails

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is an interface contact where View and data model interacts indirectly through a bridge (presenter)
 */
interface IFeedDetailsContract {

    //View
    interface IFeedDetailsView : BaseView {

        fun onFeedDetailsFetchedSuccessfully(response: FeedDetails)

        fun onFeedDetailsFetchFailure(errorMessage: String)
    }

    //presenter
    interface IPresenter {
        fun fetchFeedDetails(id: String)
    }

}