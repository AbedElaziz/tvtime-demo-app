package bjak.my.demo.base

import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import bjak.my.demo.App
import bjak.my.demo.di.component.ApplicationComponent
import butterknife.ButterKnife
import butterknife.Unbinder


/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is a base activity class where common functionality for all activities extending it can be declared
 * once.
 *
 * Here butter knife is bind on creation and cleared up on activity destroy
 */

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var unBinder: Unbinder


    protected fun getApplicationComponents(): ApplicationComponent = (application as App).applicationComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentView())
        unBinder = ButterKnife.bind(this)
        onViewReady(savedInstanceState, intent)
    }

    @CallSuper
    open fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        resolveDaggerDependency()
    }

    protected abstract fun getContentView(): Int

    protected abstract fun resolveDaggerDependency()

    override fun onDestroy() {
        unBinder.unbind()
        super.onDestroy()
    }
}