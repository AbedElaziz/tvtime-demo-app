package bjak.my.demo.base

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This is an abstract class as base of all presenters to be created in the app, where all
 * needs to have start and stop methods
 */
abstract class BasePresenter<V>(val view: V) {

    //NOT USED FOR NOW
    fun start() {}

    abstract fun stop()

}