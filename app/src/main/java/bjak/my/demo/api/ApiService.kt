package bjak.my.demo.api

import bjak.my.demo.mvp.model.FeedDetailsResponse
import bjak.my.demo.mvp.model.FeedResponse
import bjak.my.demo.utils.Constants.DISCOVER_FEED
import bjak.my.demo.utils.Constants.DISCOVER_TITLES
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Single

/**
 * Created by AbedElaziz Shehadeh on 13 Oct ,2019
 * elaziz.shehadeh@gmail.come
 *
 * This file contains api calls to the server that specifies call types and call parameters
 */

interface ApiService {

    @GET(DISCOVER_FEED)
    fun getFeeds(@Query("region") region: String,
                 @Query("page") page: Int,
                 @Query("perPage") perPage: Int) : Single<FeedResponse>

    @GET("$DISCOVER_TITLES{id}")
    fun getFeedDetails(@Path("id") id: String) : Single<FeedDetailsResponse>

}